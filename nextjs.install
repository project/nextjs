<?php

/**
 * @file
 * Install file.
 */

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

include_once "nextjs.basefields.inc";

/**
 * Install hook.
 */
function nextjs_install() {
  // Consumer fields.
  $consumerFields = array_merge(
    nextjs_basefields_consumer_cache_webhook()
  );

  foreach ($consumerFields as $name => $definition) {
    \Drupal::entityDefinitionUpdateManager()->installFieldStorageDefinition($name, 'consumer', 'nextjs', $definition);
  }
}

/**
 * Add field 'nextjs_cache_webhook_url'.
 */
function nextjs_update_10001() {
  $fields = [];
  $fields['nextjs_cache_webhook_url'] = BaseFieldDefinition::create('string')
    ->setLabel(new TranslatableMarkup('Cache Webhook URL'))
    ->setDescription(new TranslatableMarkup('URL that the webhook will be sent to. e.g. https://my-next-project.com/api/webhook/cache-revalidate.'))
    ->setDisplayOptions('view', [
      'label' => 'inline',
      'weight' => 6,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 6,
    ])
    ->setRevisionable(TRUE)
    ->setTranslatable(FALSE)
    ->setRequired(FALSE)
    ->setDefaultValue(FALSE);

  foreach ($fields as $name => $definition) {
    \Drupal::entityDefinitionUpdateManager()->installFieldStorageDefinition($name, 'consumer', 'nextjs', $definition);
  }
}

/**
 * Remove old fields `nextjs_cache_webhook_url` and `nextjs_environment`.
 */
function nextjs_update_10002() {
  $fieldsToDelete = ['nextjs_cache_webhook_path', 'nextjs_environment'];

  // Delete fields.
  foreach ($fieldsToDelete as $field) {
    FieldConfig::loadByName('consumer', 'consumer', $field)?->delete();
    FieldStorageConfig::loadByName('consumer', $field)?->delete();
  }
}
