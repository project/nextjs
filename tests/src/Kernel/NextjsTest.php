<?php

namespace Drupal\Tests\nextjs\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Test next.js module.
 */
class NextjsTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'consumers',
    'nextjs',
    'image',
    'file',
    'options',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('consumer');

    $this->drupalSetUpCurrentUser();
  }

  /**
   * Test that module can be installed.
   */
  public function testModule(): void {
    $this->assertTrue(TRUE);
  }

}
