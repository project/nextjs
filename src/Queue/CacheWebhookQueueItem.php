<?php

namespace Drupal\nextjs\Queue;

use Drupal\Component\Serialization\Json;
use Drupal\nextjs\CacheWebhookType;

/**
 * Represents a cache webhook queue item.
 */
abstract class CacheWebhookQueueItem {

  /**
   * Construct new CacheWebhookQueueItem.
   */
  public function __construct(
    protected CacheWebhookType $type,
    protected ?string $subType = NULL,
    protected array $params = [],
  ) {}

  /**
   * Get array.
   */
  public function toArray(): array {
    return [
      'type' => $this->type->value,
      'subType' => $this->subType,
      'params' => $this->params,
    ];
  }

  /**
   * Get JSON representation.
   */
  public function toJson(): string {
    return Json::encode($this->toArray());
  }

  /**
   * Construct instance from array.
   */
  abstract public static function fromArray(array $data): self;

  /**
   * Construct new CacheWebhookQueueItem from json data.
   */
  public static function fromJson(string $json) {
    $data = Json::decode($json);
    if (!isset($data['type'])) {
      throw new \InvalidArgumentException('Queue item json payload does not have field "type"!');
    }
    $type = CacheWebhookType::from($data['type']);

    return match($type) {
      CacheWebhookType::CACHE_REBUILD => CacheWebhookRebuildQueueItem::fromArray($data),
      CacheWebhookType::ENTITY => CacheWebhookEntityQueueItem::fromArray($data),
    };
  }

}
