<?php

namespace Drupal\nextjs\Queue;

use Drupal\Core\Entity\EntityInterface;
use Drupal\nextjs\CacheWebhookEntityOperation;
use Drupal\nextjs\CacheWebhookType;

/**
 * Cache webhook queue item for entity operations.
 */
class CacheWebhookEntityQueueItem extends CacheWebhookQueueItem {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    CacheWebhookEntityOperation $operation,
    string $entityTypeId,
    string|int $id,
    string $uuid,
    ?string $bundle = NULL,
  ) {
    $this->type = CacheWebhookType::ENTITY;
    $this->subType = $entityTypeId;
    $this->params = [
      'id' => $id,
      'uuid' => $uuid,
      'bundle' => $bundle,
      'operation' => $operation->value,
    ];
  }

  /**
   * Construct from entity.
   */
  public static function fromEntity(EntityInterface $entityInterface, CacheWebhookEntityOperation $operation) {
    return new static(
      $operation,
      $entityInterface->getEntityTypeId(),
      $entityInterface->id(),
      $entityInterface->uuid(),
      $entityInterface->bundle(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function fromArray(array $data): self {
    return new static(
      CacheWebhookEntityOperation::from($data['params']['operation']),
      $data['subType'],
      $data['params']['id'],
      $data['params']['uuid'],
      $data['params']['bundle'],
    );
  }

  /**
   * Get entity type id.
   */
  public function getEntityTypeId(): string {
    return $this->subType;
  }

  /**
   * Get entity ID.
   */
  public function getEntityId(): int|string {
    return $this->params['id'];
  }

  /**
   * Get entity UUID.
   */
  public function getEntityUuid(): string {
    return $this->params['uuid'];
  }

  /**
   * Get entity bundle.
   */
  public function getEntityBundle(): ?string {
    return $this->params['bundle'];
  }

  /**
   * Get entity operation.
   */
  public function getEntityOperation(): CacheWebhookEntityOperation {
    return CacheWebhookEntityOperation::from($this->params['operation']);
  }

}
