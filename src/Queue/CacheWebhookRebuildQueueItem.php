<?php

namespace Drupal\nextjs\Queue;

use Drupal\nextjs\CacheWebhookType;

/**
 * Cache webhook queue item for cache rebuild.
 */
class CacheWebhookRebuildQueueItem extends CacheWebhookQueueItem {

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->type = CacheWebhookType::CACHE_REBUILD;
    $this->subType = NULL;
    $this->params = [];
  }

  /**
   * {@inheritdoc}
   */
  public static function fromArray(array $data): self {
    return new static();
  }

}
