<?php

namespace Drupal\nextjs;

/**
 * Defines the cache webhook types.
 */
enum CacheWebhookType: string {

  case ENTITY = 'entity';
  case CACHE_REBUILD = 'cache_rebuild';

}
