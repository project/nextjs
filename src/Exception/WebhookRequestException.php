<?php

namespace Drupal\nextjs\Exception;

use Psr\Http\Message\ResponseInterface;

/**
 * Exception when webhook request fails.
 */
class WebhookRequestException extends \Exception {

  /**
   * {@inheritdoc}
   */
  public function __construct(?ResponseInterface $response = NULL) {
    if ($response) {
      parent::__construct(
        sprintf('Webhook request failed with status: %d', $response->getStatusCode()),
        code: $response->getStatusCode()
      );
    }

    parent::__construct('Webhook request failed on a network level');
  }

}
