<?php

namespace Drupal\nextjs\Webhook;

use Drupal\Component\Serialization\Json;
use Drupal\nextjs\Exception\WebhookRequestException;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Psr7\Request;
use Psr\Log\LoggerInterface;

/**
 * Service to dispatch webhook requests.
 */
abstract class WebhookDispatcher {

  /**
   * Construct new WebhookDispatcher.
   */
  public function __construct(
    protected ClientInterface $httpClient,
    protected LoggerInterface $logger,
  ) {}

  /**
   * Dispatch a webhook.
   */
  protected function sendRequest(string $url, array $payload) {
    $request = new Request(
      method: 'POST',
      uri: $url,
      headers: [
        'Content-Type' => 'application/json',
      ],
      body: Json::encode($payload)
    );

    try {
      $response = $this->httpClient->send($request, [
        'connect_timeout' => 5,
      ]);

      $this->logger->info('Successfully sent webhook to @url', [
        '@url' => $url,
      ]);

      return $response;

    }
    // Handle request errors.
    catch (ClientException $e) {
      // For every error, except not found.
      if ($e->getResponse()->getStatusCode() !== 404) {
        $this->logger->error('Webhook request failed with status @status - Payload: @payload', [
          '@status' => $e->getResponse()->getStatusCode(),
          '@payload' => $request->getBody()->__toString(),
        ]);

        throw new WebhookRequestException($e->getResponse());
      }

      // Treat not found as a warning and additionally do not throw
      // an exception here. If Next.js cannot handle the webhook, it should not
      // be handled as an error.
      $this->logger->warning('Webhook endpoint not implemented by destination: @destination', [
        '@destination' => $url,
      ]);

    }
    // Handle connection errors.
    catch (ConnectException $e) {
      $this->logger->error('Webhook request failed. Could not connect to host: @url', [
        '@url' => $url,
      ]);

      throw new WebhookRequestException();
    }
  }

}
