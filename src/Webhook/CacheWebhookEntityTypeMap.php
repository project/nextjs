<?php

namespace Drupal\nextjs\Webhook;

use Drupal\Core\Cache\Cache;

/**
 * Class to get a list of valid entity type ids for cache webhook.
 */
class CacheWebhookEntityTypeMap {

  public const CACHE_KEY = 'nextjs.cache_webhook_entity_type_map';

  /**
   * Get aggregated entity type ids for which webhooks are dispatched.
   */
  public static function getEntityTypeIds(): array {
    $cache = \Drupal::cache();

    if ($item = $cache->get(self::CACHE_KEY)) {
      return $item->data;
    }

    // This gets only the consumers which have the cache webhook enabled.
    /** @var \Drupal\consumers\Entity\ConsumerInterface[] $consumers */
    $consumers = \Drupal::entityTypeManager()->getStorage('consumer')->loadByProperties([
      'nextjs_cache_webhook_enabled' => TRUE,
    ]);

    $entityTypeIds = [];

    // Aggregate all selected entity type ids of all consumers.
    foreach ($consumers as $consumer) {
      $values = $consumer->get('nextjs_cache_webhook_entity_types')->getValue();

      $entityTypeIds = array_merge(
        $entityTypeIds,
        array_map(function ($n) {
          return $n['value'];
        }, $values),
      );
    }

    $entityTypeIds = array_unique($entityTypeIds);

    // Cache the array for future use.
    $cache->set(self::CACHE_KEY, $entityTypeIds, Cache::PERMANENT, [
      'consumer_list',
    ]);

    return $entityTypeIds;
  }

}
