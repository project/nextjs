<?php

namespace Drupal\nextjs\Webhook;

use Drupal\consumers\Entity\ConsumerInterface;
use Drupal\nextjs\Event\CacheWebhookEvent;
use Drupal\nextjs\Event\CacheWebhookResponseEvent;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * WebhookDispatcher to dispatch cache webhooks.
 */
class CacheWebhookDispatcher extends WebhookDispatcher {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    protected ClientInterface $httpClient,
    protected EventDispatcherInterface $eventDispatcher,
    protected LoggerInterface $logger,
  ) {
    parent::__construct($httpClient, $logger);
  }

  /**
   * Dispatch a given cache webhook.
   */
  public function dispatch(ConsumerInterface $consumer, array $payload) {
    // Make sure that cache webhook is enabled for consumer.
    $webhookEnabled = $consumer->get('nextjs_cache_webhook_enabled')->getString() === "1";
    if (!$webhookEnabled) {
      $this->logger->info('Not handling cache webhook for consumer with cache webhooks disabled: @consumer', [
        '@consumer' => $consumer->getClientId(),
      ]);
      return;
    }

    $url = $consumer->get('nextjs_cache_webhook_url')->getString();
    if (empty($url)) {
      $this->logger->error('Could not send webhook, because consumer @consumer does not have the webhook url set!', [
        '@consumer' => $consumer->getClientId(),
      ]);
      return;
    }

    // Let other modules modify request data.
    $event = new CacheWebhookEvent($consumer, $payload, $url);
    $this->eventDispatcher->dispatch($event, $event::EVENT_NAME);

    // Aborted.
    if ($event->isAborted()) {
      return;
    }

    // Send request.
    $response = $this->sendRequest($event->getUrl(), $event->getPayload());

    // Dispatch cache webhook response event.
    $now = new \DateTime('now');
    $event = new CacheWebhookResponseEvent($payload, $response, $url, $now);
    $this->eventDispatcher->dispatch($event, $event::EVENT_NAME);
  }

}
