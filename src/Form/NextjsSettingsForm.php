<?php

declare(strict_types=1);

namespace Drupal\nextjs\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for nextjs module.
 */
class NextjsSettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nextjs_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['info'] = [
      '#markup' => $this->t('Main configuration for Next.js integration is done via the Consumer Module.<br /><br />You can find related settings when editing the consumer for your Next.js application.'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['redirect_consumers'] = [
      '#type' => 'submit',
      '#value' => $this->t('Consumer List'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.consumer.collection');
  }

}
