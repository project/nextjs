<?php

namespace Drupal\nextjs\Plugin\QueueWorker;

use Drupal\consumers\Entity\ConsumerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\nextjs\Queue\CacheWebhookQueueItem;
use Drupal\nextjs\Webhook\CacheWebhookDispatcher;
use Psr\Container\ContainerInterface;

/**
 * Dispatch cache webhooks.
 *
 * @QueueWorker(
 *   id = "nextjs_cache_webhook",
 *   title = @Translation("Next.js cache webhook queue worker"),
 *   cron = {"time" = 30}
 * )
 */
class CacheWebhookQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  const QUEUE_NAME = 'nextjs_cache_webhook';

  /**
   * Construct new CacheWebhookQueueWorker.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected CacheWebhookDispatcher $cacheWebhookDispatcher,
    array $configuration,
    string $plugin_id,
    mixed $plugin_definition,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ): self {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('nextjs.webhook_dispatcher.cache'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $item = CacheWebhookQueueItem::fromJson($data);

    // This gets only the consumers which have the cache webhook enabled.
    /** @var \Drupal\consumers\Entity\ConsumerInterface[] $consumers */
    $consumers = $this->entityTypeManager->getStorage('consumer')->loadByProperties([
      'nextjs_cache_webhook_enabled' => TRUE,
    ]);

    foreach ($consumers as $consumer) {
      $payload = $this->createPayload($item, $consumer);

      $this->cacheWebhookDispatcher->dispatch($consumer, $payload);
    }
  }

  /**
   * Create the payload depending on settings and item.
   */
  protected function createPayload(CacheWebhookQueueItem $item, ConsumerInterface $consumer): ?array {
    return $item->toArray();
  }

}
