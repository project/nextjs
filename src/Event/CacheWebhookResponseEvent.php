<?php

namespace Drupal\nextjs\Event;

use Psr\Http\Message\ResponseInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event when a cache webhook was sent.
 */
class CacheWebhookResponseEvent extends Event {

  const EVENT_NAME = 'nextjs.cache_webhook_response';

  /**
   * Construct new event.
   */
  public function __construct(
    protected array $payload,
    protected ResponseInterface $response,
    protected string $url,
    protected \DateTime $dateTime,
  ) {}

  /**
   * If webhook was processed successfully.
   */
  public function isSuccess(): bool {
    return $this->response->getStatusCode() === 200;
  }

  /**
   * Get the payload.
   */
  public function getPayload(): array {
    return $this->payload;
  }

  /**
   * Get HTTP response.
   */
  public function getResponse(): ResponseInterface {
    return $this->response;
  }

  /**
   * Get time when webhook was processed by Next.js.
   *
   * This is useful, e.g. if you need to make sure the cache
   * revalidation is fully propagated (add 300ms to this time).
   *
   * @see https://vercel.com/docs/infrastructure/data-cache#revalidation-behavior
   */
  public function getProcessedTime(): \DateTime {
    return $this->dateTime;
  }

}
