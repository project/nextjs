<?php

namespace Drupal\nextjs\Event;

use Drupal\consumers\Entity\ConsumerInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event when a cache webhook is dispatched.
 */
class CacheWebhookEvent extends Event {

  const EVENT_NAME = 'nextjs.cache_webhook';

  /**
   * Whether the request should be aborted.
   */
  protected bool $aborted = FALSE;

  /**
   * Construct new event.
   */
  public function __construct(
    protected ConsumerInterface $consumer,
    protected array $payload,
    protected string $url,
  ) {}

  /**
   * Get corresponding consumer.
   */
  public function getConsumer(): ConsumerInterface {
    return $this->consumer;
  }

  /**
   * Get event payload.
   */
  public function getPayload(): array {
    return $this->payload;
  }

  /**
   * Set event payload.
   */
  public function setPayload(array $payload) {
    $this->payload = $payload;
  }

  /**
   * Abort the request.
   */
  public function abort() {
    $this->aborted = TRUE;
  }

  /**
   * Whether the event is marked as aborted.
   */
  public function isAborted() {
    return $this->aborted;
  }

  /**
   * Set the url.
   */
  public function setUrl(string $url) {
    $this->url = $url;
  }

  /**
   * Get the url.
   */
  public function getUrl(): string {
    return $this->url;
  }

}
