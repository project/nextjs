<?php

namespace Drupal\nextjs;

/**
 * Defines the cache webhook entity operation.
 */
enum CacheWebhookEntityOperation: string {

  case CREATE = 'create';
  case UPDATE = 'update';
  case DELETE = 'delete';

}
