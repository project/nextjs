<?php

/**
 * @file
 * Provides common basefields.
 */

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\field\FieldStorageConfigInterface;

/**
 * Provides consumer basefields for cache webhook functionality.
 */
function nextjs_basefields_consumer_cache_webhook(): array {
  $fields = [];

  // Cache Webhook.
  $fields['nextjs_cache_webhook_enabled'] = BaseFieldDefinition::create('boolean')
    ->setLabel(new TranslatableMarkup('Enable Cache Webhook'))
    ->setDescription(new TranslatableMarkup('If enabled, a webhook is sent to the Next.js instance to handle cache revalidation.'))
    ->setDisplayOptions('view', [
      'label' => 'inline',
      'weight' => 6,
    ])
    ->setDisplayOptions('form', [
      'weight' => 6,
    ])
    ->setRevisionable(TRUE)
    ->setTranslatable(FALSE)
    ->setRequired(FALSE)
    ->setDefaultValue(FALSE);

  $fields['nextjs_cache_webhook_url'] = BaseFieldDefinition::create('string')
    ->setLabel(new TranslatableMarkup('Cache Webhook URL'))
    ->setDescription(new TranslatableMarkup('URL that the webhook will be sent to. e.g. https://my-next-project.com/api/webhook/cache-revalidate.'))
    ->setDisplayOptions('view', [
      'label' => 'inline',
      'weight' => 6,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 6,
    ])
    ->setRevisionable(TRUE)
    ->setTranslatable(FALSE)
    ->setRequired(FALSE)
    ->setDefaultValue(FALSE);

  $fields['nextjs_cache_webhook_entity_types'] = BaseFieldDefinition::create('list_string')
    ->setLabel(new TranslatableMarkup('Entity types to revalidate'))
    ->setDescription(new TranslatableMarkup('Webhook is only sent for selected entity types.'))
    ->setCardinality(FieldStorageConfigInterface::CARDINALITY_UNLIMITED)
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'list_default',
      'weight' => 6,
    ])
    ->setDisplayOptions('form', [
      'type' => 'options_select',
      'weight' => 6,
    ])
    ->setSetting('allowed_values_function', 'nextjs_entity_type_values')
    ->setRevisionable(TRUE)
    ->setTranslatable(FALSE)
    ->setRequired(FALSE)
    ->setDefaultValue(FALSE);

  return $fields;
}

/**
 * Get allowed values for nextjs_environment consumer field.
 */
function nextjs_environment_allowed_values() {
  return \Drupal::service('nextjs_environment.repository')->getEnvironmentNamesAsOptions();
}

/**
 * Get allowed_values for each known entity type.
 */
function nextjs_entity_type_values() {
  $values = [];

  foreach (\Drupal::entityTypeManager()->getDefinitions() as $entityType) {
    $values[$entityType->id()] = sprintf('[%s] - %s', $entityType->id(), $entityType->getLabel());
  }

  return $values;
}
